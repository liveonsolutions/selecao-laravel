# Desenvolvedor Laravel (PHP, MySQL e Frontend)

## Introdução

Olá, se você está lendo reste documento é porque possivelmente foi selecionado para dar uma palha do que é capaz de fazer. Seja bem vindo, mão na massa!

## O Teste: Pokédex

Crie um sistema básico que permitirá a inserção de Pokémon em uma base de dados MySQL. É que nem como se fosse uma pokedéx! 😬

![Ilustração da Pokédex](http://vignette4.wikia.nocookie.net/pokepediabr/images/b/b7/Pok%C3%A9dex_GenI_Kanto.png/revision/latest?cb=20131222210519&path-prefix=pt-br)

# Funcionalidades

1. Login (não faça uma tela de cadastro, apenas login)
2. Listagem dos Pokemons: uma lista com uma imagem, numero e o nome.
3. Tocando em um pokémon, o app mostra a tela de edição
4. Tela de inserção/edição:
	- Nome
	- Resumo/Descrição curta
	- Imagem
	- Tipo (elétrico, aquático, fogo, etc...)
	- Atributos (atk, def, spd, sp-atack, sp-def, HP)
	- Habilidades (até 3): nome, força (15 a 120), descrição
	- Tudo o que você julgar de interessante (diferencial)
5. Validação de dados e arquivos (!!)
6. Excluir Pokémon
7. O design do frontend é um diferencial (jQuery, angular, etc)

# Laravel Packages?

É permitido o uso de packages, mas com moderação. Precisamos avaliar a sua competência, e não a dos desenvolvedores batutas que dão suas libs pra gente.

# Procedimento (!!)

Faça um clone deste repositório ([Seleção Laravel](https://bitbucket.org/liveonsolutions/selecao-laravel/)), crie o seu repositório e faça push para o seu remote. Dê-nos acesso ao seu repositório do Bitbucket. Quando fizer isto, avise-me por e-mail [joel@superstarselfie.io](mailto:joel@superstarselfie.io?subject=Seleção%20Laravel), avaliarei o mais rápido possível e te daremos o feedback.


*Boa sorte!*